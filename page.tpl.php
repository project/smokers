<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="<?php print $language->language; ?>" xml:lang="<?php print $language->language; ?>">

<head>
<title><?php print $head_title ?></title>
<meta http-equiv="X-UA-Compatible" content="IE=8" />
<?php print $head ?>
<?php print $styles ?>
<?php print $scripts ?>
<script type="text/javascript"><?php /* Needed to avoid Flash of Unstyle Content in IE */ ?> </script>
<?php if ($suckerfish or $suckerfish_left) { ?>
<!--[if lte IE 6]>
<script type="text/javascript" src="/<?php print $directory; ?>/js/suckerfish.js"></script>
<![endif]-->
<?php } ?>
<!--[if lte IE 6]>
<link type="text/css" rel="stylesheet" media="all" href="/<?php print $directory; ?>/css/ie6.css" />
<![endif]-->
<!--[if gte IE 7]>
<link type="text/css" rel="stylesheet" media="all" href="/<?php print $directory; ?>/css/ie7.css" />
<![endif]-->
</head>

<body class="<?php print $body_classes; ?>">

<div id="skip-nav"><a href="#main">Skip to Content</a></div>

<div class="page">
<div class="sizer">
<div class="expander0">

<div id="above" class="clearfix">
  <?php if ($above): ?><?php print $above; ?><?php endif; ?>
</div>

<div id="masthead">
<div id="header2">
  <div id="header" class="clearfix">
    <div class="header-right">
      <div class="header-left">
        <div id="logo">
          <?php if ($logo): ?>
            <a href="<?php print check_url($front_page); ?>" title="<?php print t('Home'); ?>">
              <img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" />
            </a>
          <?php endif; ?>
        </div> <!-- /logo -->
        <div id="top-elements">
          <?php print $search_box; ?>
        </div><!-- /top-elements -->
        <div id="name-and-slogan">
        <?php if ($site_name): ?>
          <h1 id="site-name">
            <a href="<?php print check_url($front_page); ?>" title="<?php print t('Home'); ?>">
              <?php print $site_name; ?>
            </a>
          </h1>
        <?php endif; ?>
        <?php if ($site_slogan): ?>
          <div id="site-slogan">
            <?php print $site_slogan; ?>
          </div>
        <?php endif; ?>
        </div> <!-- /name-and-slogan -->

      <?php if ($header): ?> 
        <?php print $header; ?>
      <?php endif; ?>

      </div> <!-- /header-left -->
    </div> <!-- /header-right -->
  </div> <!-- /header -->
</div>
</div>

<div id="prenav"></div>
<?php
if ($suckerfish) {
  include 'psuckerfish.php';
} else {
  include 'pmenu.php';
}
?>
<div id="postnav"></div>

<?php
  $section1count = 0;
  $user1count = 0;
  $user2count = 0;
  $user3count = 0;
  
  if ($user1)
  {
    $section1count++;
    $user1count++;
  }
  
  if ($user2)
  {
    $section1count++;
    $user2count++;
  }
  
  if ($user3)
  {
    $section1count++;
    $user3count++;
  }
?>

<?php if ($section1count): ?>
  <?php $section1width = 'width' . floor(99 / $section1count); ?>
  <?php $block2div = ($user1count and ($user2count or $user3count)) ? " divider" : ""; ?>
  <?php $block3div = ($user3count and ($user1count or $user2count)) ? " divider" : ""; ?>
  <div id="section1" class="clearfix">
  <table class="sections" cellspacing="0" cellpadding="0">
    <tr>
    <?php if ($user1): ?><td class="section <?php echo $section1width ?>"><?php print $user1; ?></td><?php endif; ?>  
    <?php if ($user2): ?><td class="section <?php echo $section1width . $block2div; ?>"><?php print $user2; ?></td><?php endif; ?>  
    <?php if ($user3): ?><td class="section <?php echo $section1width . $block3div; ?>"><?php print $user3; ?></td><?php endif; ?>
    </tr>
  </table>
  </div>  <!-- /section1 -->
<?php endif; ?>

<div id="middlecontainer">
<?php if ($breadcrumb) : ?>
  <div id="breadcrumb">
    <?php print $breadcrumb; ?>
  </div>
<?php endif; ?>
  <div id="wrapper">
    <div class="outer">
      <div class="float-wrap">
        <div class="colmain">
          <div id="main">
            <?php if ($mission) { ?><div id="mission"><?php print $mission ?></div><?php } ?>
            <?php if ($content_top):?><div id="content-top"><?php print $content_top; ?></div><?php endif; ?>
            <h1 class="title"><?php print $title ?></h1>
            <div class="tabs"><?php print $tabs ?></div>
            <?php print $help ?>
            <?php print $messages ?>
            <?php print $content; ?>
            <?php print $feed_icons; ?>
            <?php if ($content_bottom): ?><div id="content-bottom"><?php print $content_bottom; ?></div><?php endif; ?>
          </div>
        </div> <!-- /colmain -->
        <?php if ($left or $suckerfish_left) { ?>
          <div class="colleft">
            <?php if ($suckerfish_left) { ?>
            <div id="suckerbar" class="clearfix">
              <div id="suckerfishmenu" class="vert"><?php print $suckerfish_left ?></div>
            </div>
            <?php } ?>
            <div  id="sidebar-left"><?php print $left ?></div>
          </div>
        <?php } ?>
        <br class="brclear" />
      </div> <!-- /float-wrap -->
      <?php if ($right) { ?>
        <div class="colright">
          <div id="sidebar-right"><?php print $right ?></div>
        </div>
      <?php } ?>
      <br class="brclear" />
    </div><!-- /outer -->
  </div><!-- /wrapper -->
</div>

<div id="bar"></div>

<?php
  $section2count = 0;
  $user4count = 0;
  $user5count = 0;
  $user6count = 0;
  
  if ($user4)
  {
    $section2count++;
    $user4count++;
  }
  
  if ($user5)
  {
    $section2count++;
    $user5count++;
  }
  
  if ($user6)
  {
    $section2count++;
    $user6count++;
  }
?>

<?php if ($section2count): ?>
  <?php $section2width = 'width' . floor(99 / $section2count); ?>
  <?php $block2div = ($user4count and ($user5count or $user6count)) ? " divider" : ""; ?>
  <?php $block3div = ($user6count and ($user4count or $user5count)) ? " divider" : ""; ?>
  <div id="section2" class="clearfix">
  <table class="sections" cellspacing="0" cellpadding="0">
    <tr>
    <?php if ($user4): ?><td class="section <?php echo $section2width ?>"><?php print $user4; ?></td><?php endif; ?>  
    <?php if ($user5): ?><td class="section <?php echo $section2width . $block2div; ?>"><?php print $user5; ?></td><?php endif; ?>  
    <?php if ($user6): ?><td class="section <?php echo $section2width . $block3div; ?>"><?php print $user6; ?></td><?php endif; ?>  
    </tr>
  </table>
  </div>  <!-- /section2 -->
<?php endif; ?>

<div id="footer-wrapper" class="clearfix">
  <div id="bar2"></div>
  <div id="footer">
    <?php if (isset($primary_links)) { ?><?php print theme('links', $primary_links, array('class' =>'links', 'id' => 'navlist2')) ?><?php } ?>
    <?php if ($below) { ?><div id="below"><?php print $below; ?></div><?php } ?>
      <div class="legal">
        Copyright &copy; <?php print date('Y') ?> <a href="/"><?php print $site_name ?></a>. <?php print $footer_message ?>
        <div id="brand" class="pngfix"></div>
      </div>
  </div>
  <div class="footer-right">
    <div class="footer-left">
    </div> <!-- /footer-left -->
  </div> <!-- /footer-right -->
</div> <!-- /footer-wrapper -->

<div id="shadow" class="clearfix">
  <div class="shadow-right">
    <div class="shadow-left">

<div id="belowme">
</div>

    </div>
  </div>
</div>

</div><!-- /expander0 -->
</div><!-- /sizer -->
</div><!-- /page (optional) -->

<?php print $closure ?>
</body>
</html>
