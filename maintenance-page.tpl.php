<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="<?php print $language->language; ?>" xml:lang="<?php print $language->language; ?>">

<head>
<title><?php print $head_title ?></title>
<meta http-equiv="X-UA-Compatible" content="IE=8" />
<?php print $head ?>
<?php print $styles ?>
<?php print $scripts ?>
<script type="text/javascript"><?php /* Needed to avoid Flash of Unstyle Content in IE */ ?> </script>
<?php if ($suckerfish or $suckerfish_left) { ?>
<!--[if lte IE 6]>
<script type="text/javascript" src="/<?php print $directory; ?>/js/suckerfish.js"></script>
<![endif]-->
<?php } ?>
<!--[if lte IE 6]>
<link type="text/css" rel="stylesheet" media="all" href="/<?php print $directory; ?>/css/ie6.css" />
<![endif]-->
<!--[if gte IE 7]>
<link type="text/css" rel="stylesheet" media="all" href="/<?php print $directory; ?>/css/ie7.css" />
<![endif]-->
</head>

<body class="<?php print $body_classes; ?>">

<div class="page">
<div class="sizer">
<div class="expander0">

<div id="above" class="clearfix">
</div>

<div id="masthead">
<div id="header2">
  <div id="header" class="clearfix">
    <div class="header-right">
      <div class="header-left">
        <div id="logo">
          <?php if ($logo): ?>
            <a href="<?php print $base_path; ?>" title="<?php print t('Home'); ?>">
              <img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" />
            </a>
          <?php endif; ?>
        </div> <!-- /logo -->
        <div id="top-elements">
        </div><!-- /top-elements -->
        <div id="name-and-slogan">
        <?php if ($site_name): ?>
          <h1 id="site-name">
            <a href="<?php print $base_path ?>" title="<?php print t('Home'); ?>">
              <?php print $site_name; ?>
            </a>
          </h1>
        <?php endif; ?>
        <?php if ($site_slogan): ?>
          <div id="site-slogan">
            <?php print $site_slogan; ?>
          </div>
        <?php endif; ?>
        </div> <!-- /name-and-slogan -->
      </div> <!-- /header-left -->
    </div> <!-- /header-right -->
  </div> <!-- /header -->
</div>
</div>

<div id="prenav"></div>

<div id="postnav"></div>

<div id="middlecontainer">
  <div id="wrapper">
    <div class="outer">
      <div class="float-wrap">
        <div class="colmain">
          <div id="main">
            <h1 class="title"><?php print $title ?></h1>
            <div class="tabs"><?php print $tabs ?></div>
            <?php print $help ?>
            <?php print $messages ?>
            <?php print $content; ?>
            <?php print $feed_icons; ?>
          </div>
        </div> <!-- /colmain -->
      </div> <!-- /float-wrap -->
      <br class="brclear" />
    </div><!-- /outer -->
  </div><!-- /wrapper -->
</div>

<div id="bar"></div>

<div id="footer-wrapper" class="clearfix">
  <div id="bar2"></div>
  <div id="footer">
      <div class="legal">
        Copyright &copy; <?php print date('Y') ?> <a href="/"><?php print $site_name ?></a>.
        <div id="brand" class="pngfix"></div>
      </div>
  </div>
  <div class="footer-right">
    <div class="footer-left">
    </div> <!-- /footer-left -->
  </div> <!-- /footer-right -->
</div> <!-- /footer-wrapper -->

<div id="shadow" class="clearfix">
  <div class="shadow-right">
    <div class="shadow-left">

<div id="belowme">
</div>

    </div>
  </div>
</div>

</div><!-- /expander0 -->
</div><!-- /sizer -->
</div><!-- /page (optional) -->

<?php print $closure ?>
</body>
</html>
