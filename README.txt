README.TXT // Smokers theme for Drupal 6.

Thank you for downloading this theme!


ABOUT THE SMOKERS THEME:
-------------------------------------------------------------------------+
Smokers is an advanced theme developed to be ideal for a wide range of sites, 
been optimized for e-commerce with Ubercart. It contains the same kinds of 
features you'll find in our other professional Drupal themes.

The theme validates XHTML 1.0 Strict / CSS 2, and it is cross-browser compatible; 
works perfect in Firefox, IE 6, IE 7, IE8, Safari, Opera and Google Chrome.

Layout features
===============
- 1, 2, or 3 column layout with adaptive width (min. 1024px max.1200px at 1440px 
  disply width) based on Jello Mold Piefecta Layout (same as 0 Point theme);
  (http://www.positioniseverything.net/articles/jello.html);
- 15+1 collapsible block regions;
- built-in IE transparent PNG fix;
- jQuery CSS image preload;
  (http://www.filamentgroup.com/lab/update_automatically_preload_images_from_css_with_jquery/);
- Layout width: adaptive, fixed or fluid width;
- Round corners for page elements and primary menu;
- Block icons;
- Page icons;
- SuckerFish Drop-down primary links menu;
- Primary links menu position (center, left, right);
- Left Sidebar SuckerFish menu (for secondary or custom menu);
- Helpful body classes (unique classes for each page, term, website section, 
  language, etc.); Everything can be controlled via CSS, including each menu
  item (for static menu), how colours and other items will be displayed in
  different browsers, or by terms, sections, etc.; 
- Full breadcrumb;
- Works perfect in Multilingual installations.
- Advanced SEO theme.

Advanced theme settings features
===============================
Layout settings
- Layout width - Choose between adaptive (default), fixed or fluid width;
- Block icons - Choose between none, 32x32 pixel icons and 48x48 pixels icons (default);
- Page icons - Choose a layout with or without page icons;
- Rounded corners - Option to have rounded corners in all browsers but IE;
- jQuery CSS image preload - Choose a colour.
- Menu position - Center, left or right.

General settings
- Mission statement - Display mission statement only on front page or on all pages;
- Display or not full Breadcrumb;
- Username - Display "not verified" for unregistered usernames;
- Search results - Customize what should be displayed on the search results page.

Node settings
- Author & date - display author's username and/or date posted;
- Taxonomy terms - How to display (or not) vocabularies and category terms.

Search engine optimization (SEO) settings
- Page titles - Format the title that displays in the browser's title bar;
- Meta tags.



MODULE SUPPORT
-------------------------------------------------------------------------+
This theme can support virtualy any module.
It has been heavily tested with:
  - AdSense;
  - Advanced Forum;
  - Blockquotes;
  - CAPTCHA;
  - CCK;
  - Fivestar;
  - Gallerix;
  - Gallery Assist;
  - Google_cse;
  - Google_groups;
  - Gmaplocation;
  - i18n;
  - Image;
  - ImageCache;
  - Panels;
  - Pathauto;
  - Lightbox2;
  - Logintoboggan;
  - Print;
  - Simplenews;
  - Thickbox;
  - �BERCART;
  - Views;
  - Wysiwyg (TinyMCE and FCKeditor);
  - Weather;


  
THEME MODIFICATION
-------------------------------------------------------------------------+
Smokers theme alows many sub-themes as plugins. More sub-themes will 
be available at http://www.radut.net/drupal/

If you feel like giving the theme a look of your own, I recommend to play
with /_custom/custom-style.css; please read the comments in this file.



SIDEBARS DIMMENSIONS
-------------------------------------------------------------------------+
The maximum with available for sidebars is as follow:

                                   | left | right | both
-----------------------------------------------------------------
Variable asyimmetrical sidebars    | 250  |  250  | 160-234
-----------------------------------------------------------------

NOTE: Do not exceed the available width (especially with images) or IE will 
not behave so the sidebars may drop.  



USING THE SuckerFish DROP-DOWN MENU
-------------------------------------------------------------------------+
The menu can either be a two-level static menu or a suckerfish drop-down menu.

Out of the box the theme will show the primary and secondary menu. If you select 
(/admin/build/menu/settings) the same menu as primary links then secondary 
links will display the appropriate second level of your navigation hierarchy.

If you want to use the horizontal dropdown menus do the followings:
- go to /admin/build/block and choose the Suckerfish Menu region for the 
Primary Links block;
- go to /admin/build/menu and enable the checkbox to have all Parent Menu Items 
Expanded (this is very important);
- go to /admin/build/menu/settings and choose for "Menu containing secondary links" 
the option "No Secondary links".

If you want to have the vertical dropdown menus in the left sidebar, do as follow:
- go to /admin/build/block and choose the Suckerfish Sidebar region for any but the 
Primary Links block (for example Secondary Links block your own menu block);
- go to /admin/build/menu and enable the checkbox to have all Parent Menu Items 
Expanded (this is very important);



INSTALLATION INSTRUCTIONS
-------------------------------------------------------------------------+

1) Place the smokers directory into your themes directory (sites/all/themes/smokers).

2) Enable the Smokers theme (/admin/build/themes).

3) You can configure settings for the Smokers theme at /admin/build/themes/settings/smokers.  



UPGRADING to a new version of Smokers theme
-------------------------------------------------------------------------+

1) If possible, log on as the user with user ID 1. 

2) Put the site in "Off-line" mode.

3) Go to admin/build/themes/settings/smokers and change the theme development
   settings to "rebuild theme registry on every page". 

4) Place the smokers directory into your themes directory (sites/all/themes/smokers).
   In case you have done customization to Smokers theme, remember to overwrite theme
   custom-style.css with your custom-style.css file.

5) Configure the new settings for the Smokers theme at /admin/build/themes/settings/smokers. 

6) Clear the Drupal cache and deactivate the "rebuild theme registry on every page"
   option and put the site in "On-line" mode. It is always a good idea to refresh 
   the browser's cache (CTRL+F5).



CONTACT
-------------------------------------------------------------------------+
My drupal nick is florian <http://drupal.org/user/35316> � and I can be reached 
at florian@radut.net.

I can also be contacted for paid customizations of Smokers theme as well as
Drupal consulting, installation and customizations.

The theme is installed at: 
http://www.smokersassociation.org/
http://www.asociatiafumatorilor.ro/
